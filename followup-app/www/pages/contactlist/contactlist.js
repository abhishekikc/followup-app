angular.module("App").controller('contactListCtrl',contactListCtrl);
contactListCtrl.$inject = ['$scope', '$rootScope', '$state', '$ionicPopup', '$ionicLoading', 'AuthService', 'RestCallService'];
function contactListCtrl($scope, $rootScope, $state, $ionicPopup, $ionicLoading, AuthService, RestCallService)
{
    var userMobile = AuthService.GetUserMobile();
    
    RestCallService.makeGetCall('http://192.168.1.19:8001'+ '/users/' + userMobile + '/followupcontacts/', {}, getContactsSuccessCb, getContactsFailureCb);
    
    function getContactsSuccessCb(response) {
        console.log("Success callback in getting contacts");
        console.log(response.data.result.data);
        $scope.contacts=response.data.result.data;
        //DishesService.updateDishesList(response.data.result.data);
    }

    function getContactsFailureCb(response) {
        console.log("Failure callback in getting contacts");
        console.log(response);
        $ionicLoading.hide();
    }
    
    $scope.goToAddNewDish = function() {
        $state.go("add_new_dish");
    };
}