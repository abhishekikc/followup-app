angular.module("App").controller('contactDetailsCtrl',contactDetailsCtrl);

contactDetailsCtrl.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$ionicPopup', '$ionicLoading'];
function contactDetailsCtrl($scope, $rootScope, $state, $stateParams, $ionicPopup, $ionicLoading) {
  console.log($stateParams.contactId);

  $scope.boolFalse = false;
  $scope.boolTrue = true;

  $scope.contactFollowupData = {
    "participation": 1,
    "last_followup_date": null,
    "call_done": false,
    "volunteer_id": 1,
    "app_installed": false,
    "contact_id": 3,
    "dnw": false,
    "contact": {
      "id": 3,
      "address": "bellandur",
      "festival_area": 1,
      "language": null,
      "name": "Abhishek ",
      "username": "7897146677"
    },
    "notes": "lots of text here",
    "dos_explained": true,
    "dnd": true
  };

  $scope.saveContactFollowup = function() {
    $scope.contactFollowupData.last_followup_date = (new Date).getTime();
    console.log($scope.contactFollowupData);
  };

}
