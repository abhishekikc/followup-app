angular.module("App").controller('signInCtrl', signInCtrl);
signInCtrl.$inject = ['$scope', '$rootScope', '$state', '$http', '$ionicPopup', '$ionicLoading', 'AuthService', 'RestCallService'];

function signInCtrl($scope, $rootScope, $state, $http, $ionicPopup, $ionicLoading, AuthService, RestCallService){
    console.log("Do something");
    $scope.data = {};
    $scope.register = function() {
        if (!validateMobile($scope.data.mobile)) {
          var alertPopup = $ionicPopup.alert({
            title: "Please enter valid mobile number"
          });
          return;
        }

        var confirmPopup = $ionicPopup.confirm({
          title: "Mobile Confirmation",
          template: "<b>" + $scope.data.mobile + "</b><br/>Please confirm if this is the number you want to continue with.",
          okText: "Yes"
        }).then(function(res) {
          if (res) {
            console.log("Coming here?");
            RestCallService.makePostCall('http://192.168.1.19:8001' + '/login/volunteer/' + $scope.data.mobile, {}, {}, getUserSuccessCallback, getUserFailureCallback);
          }
        });
    };
    
    function getUserSuccessCallback(response) {
    console.log("Success callback in registration");
    console.log(response);
    AuthService.updateLocalStorage(response.data.result);
    var newDoc = response.data.result;
    newDoc._id = 'userData';
    localDB.get('userData').then(function(doc) {
      newDoc._rev = doc._rev;
      updateLocalDB(newDoc, function(response) {
        console.log("going to call state.go to contactlist");
        $state.go('contact_list', {}, {reload: true});
      });
    }).catch(function(err) {
      console.log("Error while getting userData, so going to put userData ");
      updateLocalDB(newDoc, function(response) {
        console.log("going to call state.go to contactlist");
        $state.go('contact_list', {}, {reload: true});
      });
      $state.go('contact_list', {}, {reload: true});
    });
  }

  function getUserFailureCallback(response) {
    console.log("Failure callback");
    console.log(response);
    window.localStorage.setItem('mobile', $scope.data.mobile);
    $state.go('profile', {}, {reload: true});
  }

  function updateLocalDB(doc, successCb) {
    localDB.put(doc).then(function(response) {
      console.log("Successfully put new data in pouchDB, response below");
      successCb(response);
    }).catch(function(err) {
      console.log("Error received while putting in pouchdb from registration page, error below");
      console.log(err);
    });
  }

  function updateLocalStorage(data) {
    window.localStorage.setItem('userData', JSON.stringify(data));
    window.localStorage.setItem('name', data.name);
    window.localStorage.setItem('mobile', data.mobile);
    window.localStorage.setItem('address', data.address);
    window.localStorage.setItem('language', data.language);
  }
  
    function validateMobile(mobile) {
        if (mobile.length != 10) {
          return false;
        }
        for (var i=0; i<mobile.length; i++) {
          if (!typeof(parseInt(mobile[i])) === 'number') {
            return false;
          }
        }
        return true;
    }
}