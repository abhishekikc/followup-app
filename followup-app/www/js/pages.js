angular.module("App")
    .constant ('PAGES', {
        SIGNIN_PAGE: 'pages/signin/signin.html',
        SIGNIN_JS: 'pages/signin/signin.js',
        SIGNIN_URL: '/signin',
        SIGNIN_CTRL: 'signInCtrl',

        CONTACTLIST_PAGE: 'pages/contactlist/contactlist.html',
        CONTACTLIST_JS: 'pages/contactlist/contactlist.js',
        CONTACTLIST_URL: '/contactlist',
        CONTACTLIST_CTRL: 'contactListCtrl',

        CONTACTDETAILS_PAGE: 'pages/contactdetails/contactdetails.html',
        CONTACTDETAILS_JS: 'pages/contactdetails/contactdetails.js',
        CONTACTDETAILS_URL: '/contactdetails',
        CONTACTDETAILS_CTRL: 'contactDetailsCtrl'

    });
