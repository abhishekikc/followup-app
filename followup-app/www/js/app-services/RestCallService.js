angular.module("App")
  .factory('RestCallService', RestCallService);

RestCallService.$inject = ['$http', '$ionicLoading', '$ionicPopup', '$rootScope'];
function RestCallService($http, $ionicLoading, $ionicPopup, $rootScope) {
  var retries = {};
  var MAX_RETRIES = 2;
  var service = {};

  service.makeGetCall = makeGetCall;
  service.makePostCall = makePostCall;
  service.makePutCall = makePutCall;
  service.makeDeleteCall = makeDeleteCall;
  console.log("Creating Rest Service");
  return service;

  function makeGetCall(url, config, successCallBack, failureCallBack) {
    if (!retries[url]) {
      retries[url] = 1;
    }
    $ionicLoading.show();
    $http.get(url)
      .then(function(response) {
        console.log("Received success response in RestCallService get");
        console.log(response);
        delete retries[url];
        $ionicLoading.hide();
        successCallBack(response);
      }, function(response) {
        console.log("Received failure response in RestCallService get");
        if (checkForForcedLogout(response)) {
          $ionicLoading.hide();
          forceLogout();
          return;
        }
        if (response.status <= 0) {
          retries[url] += 1;
          if (retries[url] <= MAX_RETRIES) {
            makeGetCall(url, config, successCallBack, failureCallBack);
          }
          else {
            $ionicLoading.hide();
            console.log("Problem with internet connection");
            delete retries[url];
            alertAppExit();
          }
        }
        else {
          $ionicLoading.hide();
          console.log(response);
          failureCallBack(response);
        }
      });
  }

  function makePostCall(url, data, config, successCallBack, failureCallBack) {
    if (!retries[url]) {
      retries[url] = 1;
    }
    $ionicLoading.show();
    $http.post(url, data, config)
      .then(function(response) {
        console.log("Received success response in RestCallService post call");
        delete retries[url];
        $ionicLoading.hide();
        successCallBack(response);
      }, function(response) {
        console.log("Received failure response in RestCallService post call");
        if (response.status <= 0) {
          retries[url] += 1;
          if (retries[url] <= MAX_RETRIES) {
            makePostCall(url, data, config, successCallBack, failureCallBack);
          }
          else {
            $ionicLoading.hide();
            console.log("Problem with internet connection");
            delete retries[url];
            alertAppExit();
          }
        }
        else {
          $ionicLoading.hide();
          failureCallBack(response);
        }
      });
  }

  function makePutCall(url, data, config, successCallBack, failureCallBack) {
    if (!retries[url]) {
      retries[url] = 1;
    }
    $ionicLoading.show();
    $http.put(url, data, config)
      .then(function(response) {
        console.log("Received success response in RestCallService put call");
        delete retries[url];
        $ionicLoading.hide();
        successCallBack(response);
      }, function(response) {
        console.log("Received failure response in RestCallService put call");
        if (response.status <= 0) {
          retries[url] += 1;
          if (retries[url] <= MAX_RETRIES) {
            makePostCall(url, data, config, successCallBack, failureCallBack);
          }
          else {
            $ionicLoading.hide();
            console.log("Problem with internet connection");
            delete retries[url];
            alertAppExit();
          }
        }
        else {
          $ionicLoading.hide();
          failureCallBack(response);
        }
      });
  }

  function makeDeleteCall(url, config, successCallBack, failureCallBack) {
    if (!retries[url]) {
      retries[url] = 1;
    }
    $ionicLoading.show();
    $http.delete(url)
      .then(function(response) {
        console.log("Received success response in RestCallService delete");
        delete retries[url];
        $ionicLoading.hide();
        successCallBack(response);
      }, function(response) {
        console.log("Received failure response in RestCallService delete");
        if (response.status <= 0) {
          retries[url] += 1;
          if (retries[url] <= MAX_RETRIES) {
            makeDeleteCall(url, config, successCallBack, failureCallBack);
          }
          else {
            $ionicLoading.hide();
            console.log("Problem with internet connection");
            delete retries[url];
            alertAppExit();
          }
        }
        else {
          $ionicLoading.hide();
          failureCallBack(response);
        }
      });
  }

  function alertAppExit() {
    $ionicPopup.alert({
      title: 'No internet connection',
      content: "Sorry, you can't use this app without internet connection."
    }).then(function(res) {
      ionic.Platform.exitApp();
    });
  }

  function checkForForcedLogout(response) {
    if (response.status == 401) {
      return true;
    }
    else {
      return false;
    }
  }

  function forceLogout() {
    $ionicPopup.alert({
      title: "Logout",
      template: "You are going to be logged out"
    }).then(function(res) {
      $rootScope.LogoutHelper();
      AuthService.clearLocalStorage();
    });
  }
}
