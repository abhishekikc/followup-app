angular.module("App")
  .factory('AuthService', AuthService);

function AuthService() {
  var userFields = ['name', 'mobile', 'address', 'language', 'festival_area'];
  var service = {};

  service.updateLocalStorage = updateLocalStorage;
  service.clearLocalStorage = clearLocalStorage;
  service.GetUserMobile = GetUserMobile;
  service.GetUserName = GetUserName;

  return service;

  function updateLocalStorage(data) {
    console.log("updateLocalStorage in AuthService");
    console.log(data);
    window.localStorage.setItem('userData', JSON.stringify(data));

    for (var i=0; i<userFields.length; i++) {
      var field = userFields[i];
      if (data[field]) {
        window.localStorage.setItem(field, data[field]);
      }
    }

    console.log("updated local storage in auth service");
  }

  function clearLocalStorage() {
    window.localStorage.removeItem('userData');
    for (var i=0; i<userFields.length; i++) {
      var field = userFields[i];
      window.localStorage.removeItem(field);
    }

    console.log("removed local storage in auth service");
  }

  function GetUserMobile() {
    return window.localStorage.getItem('mobile');
  }

  function GetUserName() {
    return window.localStorage.getItem('name');
  }
}
