angular.module("App")
  .factory('DishesService', DishesService);

function DishesService() {
  var dishes = [];
  var service = {};

  service.updateDishesList = updateDishesList;
  service.searchDishesByName = searchDishesByName;

  return service;

  function updateDishesList(dishesList) {
    dishes = dishesList;
  }

  function searchDishesByName(dishName) {
    dishName = dishName.trim().toLowerCase();
    for (var i=0; i<dishes.length; i++) {
      var iDishName = dishes[i].name.trim().toLowerCase();
      if (iDishName == dishName) {
        // console.log("Found a dish with same name");
        // console.log(dishes[i]);
        return dishes[i];
      }
    }
    return null;
  }
}
