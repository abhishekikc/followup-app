var localDB = new PouchDB("followup-app");

var App = angular.module('App', ['ionic'])
.run(function($ionicPlatform, $state, $ionicPopup, $ionicHistory, AuthService, RestCallService) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    console.log("App initialized");

    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $ionicPopup.confirm({
          title: 'No Internet Connection',
          content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
        })
        .then(function(result) {
          if(!result) {
            ionic.Platform.exitApp();
          }
        });
      }
    }

    localDB.get('userData').then(function(doc) {
      AuthService.updateLocalStorage(doc);
      // window.localStorage.setItem('userData', JSON.stringify(doc));
      // window.localStorage.setItem('mobile', doc.mobile);
      $state.go('contact_list');
    }).catch(function(err) {
      console.log("Error in get after ionicplatform.ready");
      console.log(err);
    });

  });

  $ionicPlatform.registerBackButtonAction(function(event) {
    if (['contact_list', 'sign_in'].indexOf($state.current.name) != -1) {
      $ionicPopup.confirm({
        title: 'Exit',
        template: 'Are you sure you want to exit?'
      }).then(function(res) {
        if (res) {
          ionic.Platform.exitApp();
        }
      });
    }
    else if ($state.current.name == 'contact_details') {
      $state.go('contact_list');
    }
  }, 100);

})

.config([
    '$httpProvider', '$stateProvider', '$urlRouterProvider','$injector', 'PAGES',

    function($httpProvider, $stateProvider, $urlRouterProvider, $injector, PAGES){

        $httpProvider.defaults.headers.post['Content-Type'] = "application/json";
        $stateProvider
        .state('sign_in',{
            url: PAGES.SIGNIN_URL,
            templateUrl: PAGES.SIGNIN_PAGE,
            controller: PAGES.SIGNIN_CTRL
        })
        .state('contact_list',{
            url: PAGES.CONTACTLIST_URL,
            templateUrl: PAGES.CONTACTLIST_PAGE,
            controller: PAGES.CONTACTLIST_CTRL
        })
        .state('contact_details',{
            url: PAGES.CONTACTDETAILS_URL + '/:contactId/',
            templateUrl: PAGES.CONTACTDETAILS_PAGE,
            controller: PAGES.CONTACTDETAILS_CTRL
        });

        $urlRouterProvider.otherwise(PAGES.SIGNIN_URL);

    }
])

.controller("AppCtrl", function AppCtrl($scope, $rootScope, $state, $ionicPopup,  $log, $ionicSideMenuDelegate){
    console.log("Inside App Controller");
    $scope.callPage = function(page) {
      $state.go(page, {}, {reload: true});
    };

})

.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return parseInt(val, 10);
      });
      ngModel.$formatters.push(function(val) {
        return '' + val;
      });
    }
  };
});
